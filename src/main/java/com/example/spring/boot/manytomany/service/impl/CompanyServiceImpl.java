package com.example.spring.boot.manytomany.service.impl;

import com.example.spring.boot.manytomany.entity.Company;
import com.example.spring.boot.manytomany.repository.CompanyRepository;
import com.example.spring.boot.manytomany.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arkady on 02.09.2015.
 */

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public Company save(Company company) {
        return companyRepository.saveAndFlush(company);
    }

    @Override
    public Company findById(int id) {
        return companyRepository.findOne(id);
    }

    @Override
    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    @Override
    public void delete(int id) {
        companyRepository.delete(id);
    }

    @Override
    public boolean exist(int id) {
        return companyRepository.exists(id);
    }

    @Override
    public Company findByName(String name) {
        return companyRepository.findByName(name);
    }
}