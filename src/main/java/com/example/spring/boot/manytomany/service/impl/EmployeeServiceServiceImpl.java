package com.example.spring.boot.manytomany.service.impl;

import com.example.spring.boot.manytomany.entity.Employee;
import com.example.spring.boot.manytomany.repository.EmployeeRepository;
import com.example.spring.boot.manytomany.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arkady on 02.09.2015.
 */

@Service
public class EmployeeServiceServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee save(Employee Employee) {
        return employeeRepository.saveAndFlush(Employee);
    }

    @Override
    public Employee findById(int id) {
        return employeeRepository.findOne(id);
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public void delete(int id) {
        employeeRepository.delete(id);
    }

    @Override
    public boolean exist(int id) {
        return employeeRepository.exists(id);
    }

    @Override
    public Employee findByName(String name) {
        return employeeRepository.findByName(name);
    }
}