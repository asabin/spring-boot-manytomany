package com.example.spring.boot.manytomany.repository;

import com.example.spring.boot.manytomany.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Arkady on 02.09.2015.
 */

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    Employee findByName(String name);
}