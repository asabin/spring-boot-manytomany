package com.example.spring.boot.manytomany.service;

import com.example.spring.boot.manytomany.entity.Company;

import java.util.List;

/**
 * Created by Arkady on 02.09.2015.
 */

public interface CompanyService {

    Company save(Company company);

    Company findById(int id);

    List<Company> findAll();

    void delete(int id);

    boolean exist(int id);

    Company findByName(String name);
}