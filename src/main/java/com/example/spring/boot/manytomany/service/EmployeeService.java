package com.example.spring.boot.manytomany.service;

import com.example.spring.boot.manytomany.entity.Employee;

import java.util.List;

/**
 * Created by Arkady on 02.09.2015.
 */

public interface EmployeeService {

    Employee save(Employee Employee);

    Employee findById(int id);

    List<Employee> findAll();

    void delete(int id);

    boolean exist(int id);

    Employee findByName(String name);
}