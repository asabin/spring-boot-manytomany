package com.example.spring.boot.manytomany;

import com.example.spring.boot.manytomany.entity.Company;
import com.example.spring.boot.manytomany.entity.Employee;
import com.example.spring.boot.manytomany.service.CompanyService;
import com.example.spring.boot.manytomany.service.EmployeeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringBootManyToManyApplication.class)
public class SpringBootManyToManyApplicationTests {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void test() {
        testEmployee();
        testCompany();
        testSaveEmployeeWithCompany();
        testSaveCompanyWithEmployee();
        testAddCompanyToEmployee();
        testAddEmployeeToCompany();
        testDeleteEmployeeWithCompany();
        testDeleteCompanyWithEmployee();
    }

//    @Test
    public void testEmployee() {
        testSaveEmployee();
        testFindEmployeeById();
        testFindAllEmployees();
        testDeleteEmployee();
        testExistEmployee();
        testFindEmployeeByName();
    }

//    @Test
    public void testCompany() {
        testSaveCompany();
        testFindCompanyById();
        testFindAllCompanies();
        testDeleteCompany();
        testExistCompany();
        testFindCompanyByName();
    }

//    @Test
    public void testSaveEmployee() {
        Employee employee = new Employee();
        employee.setName("Employee");

        employee = employeeService.save(employee);
        Assert.assertNotNull(employee);
        Assert.assertTrue(employeeService.exist(employee.getId()));
    }

//    @Test
    public void testFindEmployeeById() {
        Assert.assertNotNull(employeeService.findById(1));
    }

//    @Test
    public void testFindAllEmployees() {
        Assert.assertNotNull(employeeService.findAll());
    }

//    @Test
    public void testDeleteEmployee() {
        Employee employee = new Employee();
        employee.setName("Employee_2");

        employee = employeeService.save(employee);
        Assert.assertNotNull(employee);
        Assert.assertTrue(employeeService.exist(employee.getId()));

        employeeService.delete(employee.getId());
        Assert.assertFalse(employeeService.exist(employee.getId()));
    }

//    @Test
    public void testExistEmployee() {
        Assert.assertTrue(employeeService.exist(1));
    }

//    @Test
    public void testFindEmployeeByName() {
        Assert.assertNotNull(employeeService.findByName("Employee"));
    }

//    @Test
    public void testAddCompanyToEmployee() {
        Employee employee = employeeService.findById(1);
        Company company = companyService.findById(1);

        employee.getCompanies().add(company);
        employee = employeeService.save(employee);

        Assert.assertNotNull(employee);
        Assert.assertFalse(employee.getCompanies().isEmpty());
    }

//    @Test
    public void testSaveEmployeeWithCompany() {
        Employee employee = new Employee();
        employee.setName("Employee_3");

        Company company = companyService.findById(1);
        employee.getCompanies().add(company);

        employee = employeeService.save(employee);
        Assert.assertNotNull(employee);
        Assert.assertTrue(employeeService.exist(employee.getId()));
    }

//    @Test
    public void testDeleteEmployeeWithCompany() {
        employeeService.delete(3);
        Assert.assertFalse(employeeService.exist(3));
    }

//    @Test
    public void testSaveCompany() {
        Company company = new Company();
        company.setName("Company");

        company = companyService.save(company);
        Assert.assertNotNull(company);
        Assert.assertTrue(companyService.exist(company.getId()));
    }

//    @Test
    public void testFindCompanyById() {
        Assert.assertNotNull(companyService.findById(1));
    }

//    @Test
    public void testFindAllCompanies() {
        Assert.assertNotNull(companyService.findAll());
    }

//    @Test
    public void testDeleteCompany() {
        Company company = new Company();
        company.setName("Company_2");

        company = companyService.save(company);
        Assert.assertNotNull(company);
        Assert.assertTrue(companyService.exist(company.getId()));

        companyService.delete(company.getId());
        Assert.assertFalse(companyService.exist(company.getId()));
    }

//    @Test
    public void testExistCompany() {
        Assert.assertTrue(companyService.exist(1));
    }

//    @Test
    public void testFindCompanyByName() {
        Assert.assertNotNull(companyService.findByName("Company"));
    }

//    @Test
    public void testAddEmployeeToCompany() {
        Company company = companyService.findById(3);
        Employee employee = employeeService.findById(3);

        employee.getCompanies().add(company);   // It is needed if child is updated
        company.getEmployees().add(employee);

        company = companyService.save(company);
        Assert.assertNotNull(company);
        Assert.assertFalse(company.getEmployees().isEmpty());
    }

//    @Test
    public void testSaveCompanyWithEmployee() {
        Company company = new Company();
        company.setName("Company_3");

        Employee employee = employeeService.findById(1);
        company.getEmployees().add(employee);

        company = companyService.save(company);
        Assert.assertNotNull(company);
        Assert.assertTrue(companyService.exist(company.getId()));
    }

//    @Test
    public void testDeleteCompanyWithEmployee() {
        companyService.delete(3);
        Assert.assertFalse(companyService.exist(3));
    }
}